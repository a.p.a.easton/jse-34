package ru.easton.tm.service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.easton.tm.controller.ProjectController;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Scanner;

public abstract class AbstractService {

    protected final Scanner scanner = new Scanner(System.in);

    protected static final Logger logger = LogManager.getLogger(AbstractService.class);

    protected boolean checkAuthentication(final Long userId){
        if(userId == null){
            System.out.println("Please, log in or registry.");
            return true;
        }
        return false;
    }
}
