package ru.easton.tm.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class AbstractRepository<T> {

    protected List<T> items = new CopyOnWriteArrayList<>();

    protected final HashMap<String, List<T>> itemsMap = new HashMap<>();

    private final Logger logger = LogManager.getLogger(AbstractRepository.class);

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    protected abstract String getName(T item);

    protected void addItemToMap(final T item){
        itemsMap.putIfAbsent(getName(item), new ArrayList<>());
        itemsMap.get(getName(item)).add(item);
    }

    public List<T> findAll(){
        return items;
    }

}
