package ru.easton.tm.tread;

import ru.easton.tm.entity.Task;
import ru.easton.tm.repository.TaskRepository;

import java.time.LocalDateTime;
import java.util.List;

public class DeleteDueTaskThread implements Runnable{

    @Override
    public void run() {
        List<Task> tasks;
        while(true) {
            synchronized (TaskRepository.getInstance()) {
                tasks = TaskRepository.getInstance().getItems();
                LocalDateTime now = LocalDateTime.now();
                for (Task task : tasks) {
                    if (task.getDeadline().isBefore(now)) {
                        tasks.remove(task);
                    }
                }
            }
            try {
                Thread.currentThread().join(60000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
