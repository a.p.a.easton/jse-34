package ru.easton.tm.service;

import ru.easton.tm.entity.Project;
import ru.easton.tm.entity.Task;
import ru.easton.tm.repository.ProjectRepository;
import ru.easton.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class ProjectTaskService {

    private static ProjectTaskService instance = null;

    private final ProjectRepository projectRepository = ProjectRepository.getInstance();

    private final TaskRepository taskRepository = TaskRepository.getInstance();

    private ProjectTaskService(){
    }

    public static ProjectTaskService getInstance(){
        if (instance == null){
            instance = new ProjectTaskService();
        }
        return instance;
    }

    public List<Task> findAllByProjectId(final Long projectId, final Long userId) {
        if(projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId, userId);
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        if(projectId == null) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    public Task addTaskToProject(final Long projectId, final Long taskId, final Long userId){
        if(projectId == null) return null;
        if(taskId == null) return null;
        final Optional<Project> optionalProject = projectRepository.findById(projectId);
        if(optionalProject.isEmpty()) return null;
        final Optional<Task> optionalTask = taskRepository.findById(taskId);
        if(optionalTask.isEmpty()) return null;
        Task task = optionalTask.get();
        if(!(optionalProject.get().getUserId().equals(userId) && task.getUserId().equals(userId))) return null;
        task.setProjectId(projectId);
        return task;
    }

    public Task addTaskToProject(final Long projectId, final Long taskId){
        if(projectId == null) return null;
        if(taskId == null) return null;
        final Optional<Project> optionalProject = projectRepository.findById(projectId);
        if(optionalProject.isEmpty()) return null;
        final Optional<Task> optionalTask = taskRepository.findById(taskId);
        if(optionalTask.isEmpty()) return null;
        Task task = optionalTask.get();
        task.setProjectId(projectId);
        return task;
    }

    public Task removeTaskFromProject(final Long projectId, final Long taskId, final Long userId){
        if(projectId == null) return null;
        if(taskId == null) return null;
        final Optional<Project> optionalProject = projectRepository.findById(projectId);
        if(optionalProject.isEmpty()) return null;
        final Optional<Task> optionalTask = taskRepository.findById(taskId);
        if(optionalTask.isEmpty()) return null;
        if(!(optionalProject.get().getUserId().equals(userId) && optionalTask.get().getUserId().equals(userId))) return null;
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if(task == null) return null;
        task.setProjectId(null);
        return task;
    }

    public Task removeTaskFromProject(final Long projectId, final Long taskId){
        if(projectId == null) return null;
        if(taskId == null) return null;
        final Optional<Project> optionalProject = projectRepository.findById(projectId);
        if(optionalProject.isEmpty()) return null;
        final Optional<Task> optionalTask = taskRepository.findById(taskId);
        if(optionalTask.isEmpty()) return null;
        final Task task = taskRepository.findByProjectIdAndId(projectId, taskId);
        if(task == null) return null;
        task.setProjectId(null);
        return task;
    }

}
