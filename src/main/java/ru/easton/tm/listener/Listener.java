package ru.easton.tm.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import ru.easton.tm.exception.ProjectNotFoundException;
import ru.easton.tm.exception.TaskNotFoundException;
import ru.easton.tm.exception.UserNotFoundException;

public interface Listener {
    int listen(final String command) throws ProjectNotFoundException, JsonProcessingException, TaskNotFoundException, UserNotFoundException;
}
