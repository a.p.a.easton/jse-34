package ru.easton.tm;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.easton.tm.entity.User;
import ru.easton.tm.enumerated.Role;
import ru.easton.tm.listener.Listener;
import ru.easton.tm.listener.ListenerProjectImpl;
import ru.easton.tm.listener.ListenerTaskImpl;
import ru.easton.tm.listener.ListenerUserImpl;
import ru.easton.tm.publisher.Publisher;
import ru.easton.tm.publisher.PublisherImpl;
import ru.easton.tm.repository.ProjectRepository;
import ru.easton.tm.repository.TaskRepository;
import ru.easton.tm.service.*;
import ru.easton.tm.tread.DeleteDueTaskThread;
import ru.easton.tm.tread.TaskDueDate;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App {

    static {
        User test = UserService.getInstance().create("test", "test", Role.USER);
        User admin = UserService.getInstance().create("admin", "admin", Role.ADMIN);
        ProjectService.getInstance().create("DEMO PROJECT 1", "DEMO PROJECT 1", test.getId());
        ProjectService.getInstance().create("DEMO PROJECT 2", "DEMO PROJECT 2", admin.getId());
        ProjectService.getInstance().create("DEMO PROJECT 3", "DEMO PROJECT 3", test.getId());
        TaskService.getInstance().create("TEST TASK 1", "TEST TASK 1", test.getId());
        TaskService.getInstance().create("TEST TASK 2", "TEST TASK 2", admin.getId());
        TaskService.getInstance().create("TEST TASK 3", "TEST TASK 3", test.getId());
        TaskService.getInstance().create("TEST TASK 4", "TEST TASK 4", LocalDateTime.of(2021,01,11,17,32), test.getId());
    }



    public static void main(String[] args) {
        Publisher publisher = new PublisherImpl();
        Listener listenerUser = new ListenerUserImpl();
        Listener listenerProject = new ListenerProjectImpl();
        Listener listenerTask = new ListenerTaskImpl();
        publisher.addListener(listenerUser);
        publisher.addListener(listenerProject);
        publisher.addListener(listenerTask);
        ExecutorService executorService = Executors.newCachedThreadPool();
        TaskDueDate taskDueDate = new TaskDueDate();
        executorService.submit(taskDueDate);
        DeleteDueTaskThread deleteDueTaskThread = new DeleteDueTaskThread();
        executorService.submit(deleteDueTaskThread);
        publisher.start();
    }

    public ProjectRepository getProjectRepository(){
        return ProjectRepository.getInstance();
    }

    public TaskRepository getTaskRepository(){
        return TaskRepository.getInstance();
    }

    public ProjectTaskService getProjectTaskService(){
        return ProjectTaskService.getInstance();
    }
}
