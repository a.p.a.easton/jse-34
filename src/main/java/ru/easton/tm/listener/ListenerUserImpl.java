package ru.easton.tm.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import ru.easton.tm.exception.UserNotFoundException;
import ru.easton.tm.service.UserService;

import static ru.easton.tm.constant.TerminalConst.*;

public class ListenerUserImpl implements Listener{

    private final UserService userService = UserService.getInstance();

    @Override
    public int listen(String command) throws JsonProcessingException, UserNotFoundException {
        switch (command){
            case USER_CREATE: return userService.createUser();
            case USER_CLEAR: return userService.clearUser();
            case USER_LIST: return userService.listUser();
            case USER_VIEW: return userService.viewUserById();
            case USER_UPDATE: return userService.updateUserById();
            case USER_REMOVE_BY_ID: return userService.removeById();
            case USER_CHANGE_PASSWORD: return userService.changePassword();
            case USER_SAVE: return userService.saveUsers();
            case USER_IMPORT: return userService.importUsers();
            case USER_SIGN_IN:
            userService.signIn();
            return 0;
            case USER_SIGN_OUT:
            userService.signOut();
            return 0;
            default: return -1;
        }
    }

}
