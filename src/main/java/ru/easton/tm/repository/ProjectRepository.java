package ru.easton.tm.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.easton.tm.entity.Project;
import ru.easton.tm.exception.ProjectNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProjectRepository extends AbstractRepository<Project>{

    private static ProjectRepository instance = null;

    private final Logger logger = LogManager.getLogger(ProjectRepository.class);

    private ProjectRepository(){
    }

    public static ProjectRepository getInstance(){
        if (instance == null){
            instance = new ProjectRepository();
        }
        return instance;
    }

    @Override
    protected String getName(Project item) {
        return item.getName();
    }

    public Project create(final String name){
        final var project = new Project(name);
        items.add(project);
        addItemToMap(project);
        return project;
    }

    public Project create(final String name, final String description, final Long userId){
        logger.trace("create --> name: {}, description: {}, userId: {}", name, description, userId);
        final var project = new Project(name, description);
        project.setUserId(userId);
        items.add(project);
        addItemToMap(project);
        logger.trace("create --> {}", project);
        return project;
    }

    public Optional<Project> update(final Long id, final String name, final String description){
        logger.trace("update --> id: {}, name: {}, description: {}", id, name, description);
        final Optional<Project> optionalProject = findById(id);
        if(optionalProject.isEmpty()) return optionalProject;
        var project = optionalProject.get();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        logger.trace("update --> {}", project);
        return Optional.of(project);
    }

    public void clear(final Long userId) throws ProjectNotFoundException {
        logger.trace("clear --> userId: {}", userId);
        List<Project> userProjects = findByUserId(userId);
        if(userProjects.isEmpty()) throw new ProjectNotFoundException("You don't have any project");
        for(final Project project: userProjects){
            itemsMap.get(project.getName()).remove(project);
            if(itemsMap.get(project.getName()).isEmpty())
                itemsMap.remove(project.getName());
            items.remove(project);
        }
    }

    public List<Project> findByUserId(final Long userId) {
        logger.trace("findByUserId --> userId: {}", userId);
        List<Project> userProjects = new ArrayList<>();
        for(final Project project: items){
            if(project.getUserId() == null) continue;
            if(project.getUserId().equals(userId)) userProjects.add(project);
        }
        logger.trace("findByUserId --> {}", userProjects);
        return userProjects;
    }

    public Project findByIndex(int index, Long userId) throws ProjectNotFoundException {
        logger.trace("findByIndex --> index: {}, userId: {}", index, userId);
        List<Project> userProjects = findByUserId(userId);
        if(index > userProjects.size() - 1) throw new ProjectNotFoundException("The entered index is greater than the number of your projects");
        logger.trace("findByIndex --> {}", userProjects.get(index));
        return userProjects.get(index);
    }

    public Project findByIndex(int index) throws ProjectNotFoundException {
        if(index > items.size() - 1) throw new ProjectNotFoundException("The entered index is greater than the number of projects");
        return items.get(index);
    }

    public List<Project> findByName(final String name, final Long userId) throws ProjectNotFoundException{
        logger.trace("findByName --> name: {}, userId: {}", name, userId);
        List<Project> projectsByName = itemsMap.get(name);
        if(projectsByName == null) throw new ProjectNotFoundException("Projects with entered name not found");
        List<Project> result = new ArrayList<>();
        for(final Project project: projectsByName){
            if(project.getUserId()==null) continue;
            if(project.getUserId().equals(userId)) result.add(project);
        }
        if(result.isEmpty()) throw new ProjectNotFoundException(String.format("You don't have projects with name \"%s\"", name));
        logger.trace("findByName --> {}", result);
        return result;
    }

    public Optional<Project> findById(final Long id){
        for(final Project project: items){
            if(project.getId().equals(id)) return Optional.of(project);
        }
        return Optional.empty();
    }

    public void removeByIndex(final int index, final Long userId) throws ProjectNotFoundException {
        logger.trace("removeByIndex --> index: {}, userId: {}", index, userId);
        final var project = findByIndex(index, userId);
        if(project==null) throw new ProjectNotFoundException(String.format("You don't have projects with index \"%s\"", index));
        items.remove(project);
    }

    public void removeByName(final String name, Long userId) throws ProjectNotFoundException {
        logger.trace("removeByName --> name: {}, userId: {}", name, userId);
        boolean remove = items.removeIf(x -> x.getName().equals(name) && x.getUserId().equals(userId));
        if(!remove) throw new ProjectNotFoundException(String.format("You don't have projects with name \"%s\"", name));
    }

    public void removeById(final Long id) throws ProjectNotFoundException {
        logger.trace("removeById --> id: {}", id);
        boolean remove = items.removeIf(x -> x.getId().equals(id));
        if(!remove) throw new ProjectNotFoundException(String.format("You don't have projects with id \"%s\"", id));
    }

}
