package ru.easton.tm.publisher;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.easton.tm.exception.ProjectNotFoundException;
import ru.easton.tm.exception.TaskNotFoundException;
import ru.easton.tm.exception.UserNotFoundException;
import ru.easton.tm.listener.Listener;
import ru.easton.tm.service.SystemService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static ru.easton.tm.constant.TerminalConst.EXIT;

public class PublisherImpl implements Publisher{

    List<Listener> listeners = new ArrayList<>();

    private final SystemService systemService = SystemService.getInstance();

    private final Logger logger = LogManager.getLogger(PublisherImpl.class);

    @Override
    public void addListener(Listener listener) {
        if(!listeners.contains(listener)){
            listeners.add(listener);
        }
    }

    @Override
    public void deleteListener(Listener listener) {
        listeners.remove(listener);
    }

    @Override
    public void listen(String command) throws ProjectNotFoundException, TaskNotFoundException, JsonProcessingException, UserNotFoundException {
        for(Listener listener: listeners){
            if(listener.listen(command)==0){
                return;
            }
        }
        SystemService.getInstance().displayError();
    }

    @Override
    public void start() {
        systemService.displayWelcome();
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while(!EXIT.equals(command)){
            command = scanner.nextLine();
            systemService.saveCommand(command);
            try {
                listen(command);
            } catch (ProjectNotFoundException | TaskNotFoundException | JsonProcessingException | UserNotFoundException e) {
                logger.error(e.getMessage());
            }
        }
    }

}
