package ru.easton.tm.controller;

import ru.easton.tm.entity.User;
import ru.easton.tm.enumerated.Role;
import ru.easton.tm.exception.UserNotFoundException;
import ru.easton.tm.service.UserService;

import java.util.List;

public class UserController extends AbstractController{

    private final UserService userService = UserService.getInstance();

    public int createUser(){
        System.out.println("[CREATE USER]");
        System.out.println("PLEASE, ENTER LOGIN: ");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD: ");
        final String password = scanner.nextLine();
        final User user = userService.create(login, password);
        if(user == null) System.out.println("FAIL");
        System.out.println("OK");
        return 0;
    }

    public int clearUser() {
        System.out.println("[CLEAR USER]");
        userService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int listUser() {
        System.out.println("[LIST USERS]");
        viewUsers(userService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    private void viewUser(final User user){
        if(user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("[OK]");
    }

    public void viewUsers(final List<User> users){
        int index = 1;
        for(final User user: users){
            System.out.println(user.toString());
            index++;
        }
    }

    public int viewUserById() {
        System.out.println("[PLEASE, ENTER ID]");
        final Long id;
        try {
            id = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("[ENTER NUMBER, NOT CHAR]");
            return 0;
        }
        final User user = userService.findById(id);
        if(user == null){
            System.out.println("[USER NOT FOUND]");
            return 0;
        }
        viewUser(user);
        return 0;
    }

    public int updateUserById() {
        System.out.println("[PLEASE, ENTER ID]");
        final Long id;
        try {
            id = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("[ENTER NUMBER, NOT CHAR]");
            return 0;
        }
        System.out.println("[PLEASE, ENTER LAST NAME]");
        final String lastName = scanner.nextLine();
        System.out.println("[PLEASE, ENTER FIRST NAME]");
        final String firstName = scanner.nextLine();
        System.out.println("[PLEASE, ENTER MIDDLE NAME]");
        final String middleName = scanner.nextLine();
        final User user = userService.update(id, firstName, lastName, middleName);
        if(user == null) {
            System.out.println("[USER NOT FOUND]");
            return 0;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int removeById() throws UserNotFoundException {
        System.out.println("[REMOVE USER BY ID]");
        System.out.println("PLEASE, ENTER USER ID: ");
        Long id = null;
        if(scanner.hasNextLong())
            id = scanner.nextLong();
        userService.removeById(id);
        System.out.println("[OK]");
        return 0;
    }

    public Long signIn() {
        System.out.println("[ENTER LOGIN]");
        final String login = scanner.nextLine();
        System.out.println("[ENTER PASSWORD]");
        final String password = scanner.nextLine();
        final User user = userService.signIn(login, password);
        if(user == null) {
            System.out.println("[FAIL]");
            return null;
        }
        System.out.println("[OK]");
        return user.getId();
    }

    public Long signOut() {
        System.out.println("[SUCCESS SIGN OUT]");
        return null;
    }

    public int changePassword(final Long userId) {
        if(checkAuthentication(userId)) return 0;
        User user = userService.findById(userId);
        if(user.getRole().equals(Role.USER)) {
            System.out.println("[ENTER NEW PASSWORD]");
            final String password = scanner.nextLine();
            user = userService.changePassword(user, password);
        }
        else if(user.getRole().equals(Role.ADMIN)){
            System.out.println("[ENTER USER LOGIN]");
            final String login = scanner.nextLine();
            System.out.println("[ENTER NEW PASSWORD]");
            final String password = scanner.nextLine();
            user = userService.findByLogin(login);
            if(user == null) {
                System.out.println("[FAIL]");
                return 0;
            }
            user = userService.changePassword(user, password);
        }
        if(user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[OK]");
        return 0;
    }

}
