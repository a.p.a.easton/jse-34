package ru.easton.tm.service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.easton.tm.entity.User;
import ru.easton.tm.enumerated.Role;
import ru.easton.tm.exception.UserNotFoundException;
import ru.easton.tm.repository.UserRepository;
import ru.easton.tm.util.HashUtil;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class UserService extends AbstractService{

    private static final String fileNameJson = "users.json";

    private static final String fileNameXml = "users.xml";

    private static UserService instance = null;

    private User currentUser;

    private UserRepository userRepository = UserRepository.getInstance();

    private UserService(){
    }

    public static UserService getInstance(){
        if(instance == null){
            instance = new UserService();
        }
        return instance;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public User signIn(final String login, final String password) {
        if(login == null || login.isEmpty()) return null;
        if(password == null || password.isEmpty()) return null;
        final User user = findByLogin(login);
        if(user == null) return null;
        final var passwordHash = HashUtil.md5(password);
        if(!user.getPasswordHash().equals(passwordHash)) return null;
        return user;
    }

    public User create(String login, String password) {
        login = login.trim();
        password = password.trim();
        if(login == null || login.isEmpty()) return null;
        if(password == null || password.isEmpty()) return null;
        if(existByLogin(login)) return null;
        final var passwordHash = HashUtil.md5(password);
        final var user = new User(login, passwordHash);
        userRepository.create(user);
        return user;
    }

    public User create(final String login, final String password, final Role role) {
        final var user = create(login, password);
        if(user == null) return null;
        user.setRole(role);
        return user;
    }

    public User update(final Long id, final String firstName, final String lastName, final String middleName) {
        final User user = findById(id);
        if(user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    public void clear() {
        userRepository.clear();
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findByIndex(final int index) {
        if(index < 0) return null;
        return userRepository.findByIndex(index);
    }

    public User findById(final Long id) {
        if(id == null) return null;
        return userRepository.findById(id);
    }

    public User findByLogin(final String login){
        return userRepository.findByLogin(login);
    }

    public void removeById(final Long id) throws UserNotFoundException {
        if(id == null) throw new UserNotFoundException("You don't specified the user id");
        userRepository.removeById(id);
    }

    public User removeByIndex(final int index) {
        if(index < 0) return null;
        return userRepository.removeByIndex(index);
    }

    public boolean existByLogin(final String login){
        return userRepository.existByLogin(login);
    }

    public User changePassword(final User user, String password) {
        if(password == null) return null;
        password = password.trim();
        if(password.isEmpty()) return null;
        final var passwordHash = HashUtil.md5(password);
        if(user == null) return null;
        user.setPasswordHash(passwordHash);
        return user;
    }

    public int createUser(){
        System.out.println("[CREATE USER]");
        System.out.println("PLEASE, ENTER LOGIN: ");
        final var login = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD: ");
        final var password = scanner.nextLine();
        final User user = create(login, password);
        if(user == null) System.out.println("FAIL");
        System.out.println("OK");
        return 0;
    }

    public int clearUser() {
        System.out.println("[CLEAR USER]");
        clear();
        System.out.println("[OK]");
        return 0;
    }

    public int listUser() {
        System.out.println("[LIST USERS]");
        viewUsers(findAll());
        System.out.println("[OK]");
        return 0;
    }

    private void viewUser(final User user){
        if(user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("[OK]");
    }

    public void viewUsers(final List<User> users){
        int index = 1;
        for(final User user: users){
            System.out.println(user.toString());
            index++;
        }
    }

    public int viewUserById() {
        System.out.println("[PLEASE, ENTER ID]");
        final Long id;
        try {
            id = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("[ENTER NUMBER, NOT CHAR]");
            return 0;
        }
        final User user = findById(id);
        if(user == null){
            System.out.println("[USER NOT FOUND]");
            return 0;
        }
        viewUser(user);
        return 0;
    }

    public int updateUserById() {
        System.out.println("[PLEASE, ENTER ID]");
        final Long id;
        try {
            id = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("[ENTER NUMBER, NOT CHAR]");
            return 0;
        }
        System.out.println("[PLEASE, ENTER LAST NAME]");
        final String lastName = scanner.nextLine();
        System.out.println("[PLEASE, ENTER FIRST NAME]");
        final String firstName = scanner.nextLine();
        System.out.println("[PLEASE, ENTER MIDDLE NAME]");
        final String middleName = scanner.nextLine();
        final User user = update(id, firstName, lastName, middleName);
        if(user == null) {
            System.out.println("[USER NOT FOUND]");
            return 0;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int removeById() throws UserNotFoundException {
        System.out.println("[REMOVE USER BY ID]");
        System.out.println("PLEASE, ENTER USER ID: ");
        Long id = null;
        if(scanner.hasNextLong())
            id = scanner.nextLong();
        removeById(id);
        System.out.println("[OK]");
        return 0;
    }

    public void signIn() {
        System.out.println("[ENTER LOGIN]");
        final var login = scanner.nextLine();
        System.out.println("[ENTER PASSWORD]");
        final var password = scanner.nextLine();
        final User user = signIn(login, password);
        if(user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
        currentUser = user;
    }

    public void signOut() {
        currentUser = null;
        System.out.println("[SUCCESS SIGN OUT]");
    }

    public int changePassword() {
        if(checkAuthentication(currentUser.getId())) return 0;
        User user = findById(currentUser.getId());
        if(user.getRole().equals(Role.USER)) {
            System.out.println("[ENTER NEW PASSWORD]");
            final String password = scanner.nextLine();
            user = changePassword(user, password);
        }
        else if(user.getRole().equals(Role.ADMIN)){
            System.out.println("[ENTER USER LOGIN]");
            final String login = scanner.nextLine();
            System.out.println("[ENTER NEW PASSWORD]");
            final String password = scanner.nextLine();
            user = findByLogin(login);
            if(user == null) {
                System.out.println("[FAIL]");
                return 0;
            }
            user = changePassword(user, password);
        }
        if(user == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int saveUsers() throws JsonProcessingException {
        saveUsersAsJson();
        saveUsersAsXml();
        System.out.println("[OK]");
        return 0;
    }

    public void saveUsersAsJson() throws JsonProcessingException {
        var objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(UserService.getInstance().getUserRepository().getUsers());
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileNameJson))){
            objectOutputStream.writeUTF(json);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveUsersAsXml(){
        var xmlMapper = new XmlMapper();
        try {
            xmlMapper.writerWithDefaultPrettyPrinter().writeValue(new File(fileNameXml), UserService.getInstance().getUserRepository().getUsers());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int importUsers() {
        String json;
        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileNameJson))) {
            json = objectInputStream.readUTF();
            var objectMapper = new ObjectMapper();
            objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            List<User> users = Arrays.asList(objectMapper.readValue(json, User[].class));
            userRepository.setUsers(users);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("[OK]");
        return 0;
    }
}
