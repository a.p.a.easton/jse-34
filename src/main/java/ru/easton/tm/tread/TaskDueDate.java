package ru.easton.tm.tread;

import ru.easton.tm.entity.Task;
import ru.easton.tm.service.TaskService;
import ru.easton.tm.service.UserService;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

public class TaskDueDate implements Runnable{
    private final UserService userService = UserService.getInstance();
    private final TaskService taskService = TaskService.getInstance();

    @Override
    public void run() {
        List<Task> tasks;
        while(true){
            synchronized (userService){
                if(userService.getCurrentUser() == null) {
                    continue;
                }
                tasks = taskService.findByUserId(userService.getCurrentUser().getId());
            }
            LocalDateTime now = LocalDateTime.now();
            for(Task task: tasks){
                LocalDateTime notNow = task.getDeadline();
                Duration duration = Duration.between(now, notNow);
                //System.out.println("Now: " + now + ", notNow: " + notNow + ", duration: " + duration.getSeconds());
                if(duration.toMinutes() == 240){
                    System.out.println(String.format("The task %s will be expired in %d hours", task.getName(), duration.toHours() % 24));
                }
                else if(duration.toMinutes() == 60){
                    System.out.println(String.format("The task %s will be expired in %d hours", task.getName(), duration.toHours() % 24));
                }
                else if(duration.toSeconds() == 1800){
                    System.out.println(String.format("The task %s will be expired in %d minutes", task.getName(), duration.toMinutes() % 60));
                }
                else if(duration.toSeconds() == 900){
                    System.out.println(String.format("The task %s will be expired in %d minutes", task.getName(), duration.toMinutes() % 60));
                }
                else if(duration.toSeconds() == 300){
                    System.out.println(String.format("The task %s will be expired in %d minutes", task.getName(), duration.toMinutes() % 60));
                }
            }
            try {
                Thread.currentThread().join(60000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
