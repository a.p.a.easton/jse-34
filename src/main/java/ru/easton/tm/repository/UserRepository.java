package ru.easton.tm.repository;

import ru.easton.tm.entity.User;
import ru.easton.tm.exception.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private static UserRepository instance = null;

    private List<User> users = new ArrayList<>();

    private UserRepository(){
    }

    public static UserRepository getInstance(){
        if(instance == null){
            instance = new UserRepository();
        }
        return instance;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void create(final User user) {
        users.add(user);
    }

    public void clear() {
        users.clear();
    }

    public List<User> findAll() {
        return users;
    }

    public User findByIndex(int index){
        if(index > users.size() - 1) return null;
        return users.get(index);
    }

    public User findById(final Long id){
        for(final User user: users){
            if(user.getId().equals(id)) return user;
        }
        return null;
    }

    public User findByLogin(final String login){
        for(final User user: users){
            if(user.getLogin().equals(login)) return user;
        }
        return null;
    }

    public void removeById(final Long id) throws UserNotFoundException {
        boolean remove = users.removeIf(x -> x.getId().equals(id));
        if(!remove) throw new UserNotFoundException(String.format("User with %s is not found", id));
    }

    public User removeByIndex(final int index){
        final var user = findByIndex(index);
        if(user==null) return null;
        users.remove(user);
        return user;
    }

    public boolean existByLogin(String login) {
        final var user = findByLogin(login);
        return user != null;
    }
}
