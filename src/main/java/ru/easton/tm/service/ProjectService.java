package ru.easton.tm.service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.easton.tm.entity.Project;
import ru.easton.tm.exception.ProjectNotFoundException;
import ru.easton.tm.repository.ProjectRepository;

import java.io.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProjectService extends AbstractService{

    private static final String fileNameJson = "projects.json";

    private static final String fileNameXml = "projects.xml";

    private static ProjectService instance = null;

    private final ProjectRepository projectRepository = ProjectRepository.getInstance();

    private final UserService userService = UserService.getInstance();

    private ProjectService(){
    }

    public static ProjectService getInstance(){
        if (instance == null){
            instance = new ProjectService();
        }
        return instance;
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public Project create(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public Project create(String name, String description, Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description, userId);
    }

    public Project update(Long id, String name, String description) throws ProjectNotFoundException{
        if (name == null || name.isEmpty()) throw new ProjectNotFoundException("Project's name can't be empty");
        if (description == null || description.isEmpty()) throw new ProjectNotFoundException("Project's description can't be empty");
        return projectRepository.update(id, name, description).orElseThrow(() -> new ProjectNotFoundException(String.format("You don't have projects with id \"%s\"", id)));
    }

    public void clear(final Long userId) throws ProjectNotFoundException {
        projectRepository.clear(userId);
    }

    public List<Project> findByUserId(final Long userId) {
        return projectRepository.findByUserId(userId);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project findByIndex(final int index, final Long userId) throws ProjectNotFoundException {
        if(index < 0) throw new ProjectNotFoundException("Entered index less than 0");
        return projectRepository.findByIndex(index, userId);
    }

    public Project findByIndex(final int index) throws ProjectNotFoundException {
        if(index < 0) throw new ProjectNotFoundException("Entered index less than 0");
        return projectRepository.findByIndex(index);
    }

    public List<Project> findByName(final String name, final Long userId) throws ProjectNotFoundException{
        if(name == null || name.isEmpty()) throw new ProjectNotFoundException("Project's name can't be empty");
        return projectRepository.findByName(name, userId);
    }

    public void removeByIndex(int index, final Long userId) throws ProjectNotFoundException {
        if(index < 0) throw new ProjectNotFoundException("Entered index less than 0");
        projectRepository.removeByIndex(index, userId);
    }

    public void removeByName(String name, Long userId) throws ProjectNotFoundException {
        if (name == null || name.isEmpty()) throw new ProjectNotFoundException("Project's name can't be empty");
        projectRepository.removeByName(name, userId);
    }

    public void removeById(final Long id) throws ProjectNotFoundException{
        if (id == null) throw new ProjectNotFoundException("Id can't be null");
        projectRepository.removeById(id);
    }

    public int createProject(){
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT NAME: ");
        final var name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION: ");
        final var description = scanner.nextLine();
        create(name, description, userId);
        System.out.println("[OK]");
        logger.info("Project was successfully created");
        return 0;
    }

    public int viewProjectByIndex() throws ProjectNotFoundException {
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("ENTER, PROJECT INDEX:");
        int index = -1;
        try{
            index = Integer.parseInt(scanner.nextLine()) - 1;
        }
        catch (NumberFormatException e){
            System.out.println("ERROR, INCORRECT INDEX");
        }
        final Project project = findByIndex(index, userId);
        viewProject(project);
        return 0;
    }

    public int viewProjectByName() throws ProjectNotFoundException {
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final var name = scanner.nextLine();
        final List<Project> projects = findByName(name, userId);
        viewProject(projects);
        return 0;
    }

    public int updateProjectByIndex() throws ProjectNotFoundException {
        final var userId = userService.getCurrentUser().getId();
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER, PROJECT INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Project project = findByIndex(index, userId);
        System.out.println("PLEASE, ENTER PROJECT NAME: ");
        final var name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION: ");
        final var description = scanner.nextLine();
        update(project.getId(), name, description);
        System.out.println("[OK]");
        logger.info("Project was successfully updated");
        return 0;
    }

    public int removeProjectByName() throws ProjectNotFoundException {
        final var userId = userService.getCurrentUser().getId();
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("PLEASE, ENTER PROJECT NAME: ");
        final var name = scanner.nextLine();
        removeByName(name, userId);
        System.out.println("[OK]");
        logger.info("Project was successfully removed");
        return 0;
    }

    public int removeProjectById() throws ProjectNotFoundException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID: ");
        Long id = null;
        if(scanner.hasNextLong())
            id = scanner.nextLong();
        removeById(id);
        System.out.println("[OK]");
        logger.info("Project was successfully removed");
        return 0;
    }

    public int removeProjectByIndex() throws ProjectNotFoundException {
        final var userId = userService.getCurrentUser().getId();
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("PLEASE, ENTER PROJECT INDEX: ");
        final int index = scanner.nextInt() - 1;
        removeByIndex(index, userId);
        System.out.println("[OK]");
        logger.info("Project was successfully removed");
        return 0;
    }

    public int clearProject() throws ProjectNotFoundException {
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("[CLEAR PROJECT]");
        clear(userId);
        System.out.println("[OK]");
        logger.info("Projects was successfully cleared");
        return 0;
    }

    public int listProject() throws ProjectNotFoundException {
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("[LIST PROJECT]");
        final List<Project> projects = findByUserId(userId);
        if(projects.isEmpty()) throw new ProjectNotFoundException("You don't have any projects");
        Collections.sort(projects, Comparator.comparing(Project::getName));
        int index = 1;
        for(final Project project: projects){
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        return 0;
    }

    private void viewProject(final Project project){
        if(project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    private void viewProject(final List<Project> projects){
        for(final Project project: projects) {
            System.out.println("[VIEW PROJECT]");
            System.out.println("ID: " + project.getId());
            System.out.println("NAME: " + project.getName());
            System.out.println("DESCRIPTION: " + project.getDescription());
            System.out.println("[OK]");
        }
    }

    public int saveProjects() throws JsonProcessingException {
        saveProjectsAsJson();
        saveProjectsAsXml();
        System.out.println("[OK]");
        return 0;
    }

    public void saveProjectsAsJson() throws JsonProcessingException {
        var objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(ProjectService.getInstance().getProjectRepository().getItems());
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileNameJson))){
            objectOutputStream.writeUTF(json);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveProjectsAsXml(){
        var xmlMapper = new XmlMapper();
        try {
            xmlMapper.writerWithDefaultPrettyPrinter().writeValue(new File(fileNameXml), ProjectService.getInstance().getProjectRepository().getItems());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public int importProjects(){
        String json;
        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileNameJson))) {
            json = objectInputStream.readUTF();
            var objectMapper = new ObjectMapper();
            objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            List<Project> projects = Arrays.asList(objectMapper.readValue(json, Project[].class));
            projectRepository.setItems(projects);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("[OK]");
        return 0;
    }

}
