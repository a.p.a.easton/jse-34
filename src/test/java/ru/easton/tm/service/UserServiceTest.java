package ru.easton.tm.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.easton.tm.entity.User;
import ru.easton.tm.exception.UserNotFoundException;
import ru.easton.tm.repository.UserRepository;
import ru.easton.tm.util.HashUtil;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class UserServiceTest {

    private UserService userService;

    private final UserService mock = Mockito.mock(UserService.class);

    private final UserRepository userRepository = Mockito.mock(UserRepository.class);

    final User user = new User("test", HashUtil.md5("test"));

    @BeforeEach
    void setup(){
        userService = UserService.getInstance();
    }

    @AfterEach
    void clear(){
        userService.clear();
    }

    @Test
    void createEmptyArgument() {
        assertNull(userService.create(" ", "dffsd"));
        assertNull(userService.create("test ", ""));
    }

    @Test
    void createUserExist() {
        userService.create("test", "test");
        assertNull(userService.create("test", "test"));
    }

    @Test
    void updateUserIsFound() {
        User actual = userService.create("test", "test");
        when(mock.findById(actual.getId())).thenReturn(actual);
        assertEquals(actual, userService.update(actual.getId(),"","",""));
    }

    @Test
    void updateUserNotFound() {
        assertNull(userService.update(user.getId(),"","",""));
    }

    @Test
    void updateUserNormal() {
        user.setFirstName("Иван");
        user.setLastName("Иванов");
        user.setMiddleName("Иванович");
        User actual = userService.create("test", "test");
        User result = userService.update(actual.getId(),"Иван","Иванов","Иванович");
        assertEquals(user.getFirstName(), result.getFirstName());
    }

    @Test
    void findByLogin()  {
        assertNull(userService.findByLogin(null));
        assertNull(userService.findByLogin(""));
        userService.setUserRepository(userRepository);
        when(userRepository.findByLogin("test")).thenReturn(user);
        assertEquals(user,userService.findByLogin("test"));
        verify(userRepository).findByLogin("test");
    }

    @Test
    void removeById(){
        assertThrows(UserNotFoundException.class, () -> userService.removeById(null));
    }

    @Test
    void changePassword(){
        assertNull(userService.changePassword(user, null));
        assertNull(userService.changePassword(user," "));
        assertNull(userService.changePassword(null, "123"));
        assertEquals(user, userService.changePassword(user, "123"));
    }
}