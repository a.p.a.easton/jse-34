package ru.easton.tm.controller;

import java.util.Scanner;

public class AbstractController {

    protected final Scanner scanner = new Scanner(System.in);

    protected boolean checkAuthentication(final Long userId){
        if(userId == null){
            System.out.println("Access is denied.");
            return true;
        }
        return false;
    }

}
