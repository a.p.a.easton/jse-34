package ru.easton.tm.publisher;

import com.fasterxml.jackson.core.JsonProcessingException;
import ru.easton.tm.exception.ProjectNotFoundException;
import ru.easton.tm.exception.TaskNotFoundException;
import ru.easton.tm.exception.UserNotFoundException;
import ru.easton.tm.listener.Listener;

public interface Publisher {
    void addListener(Listener listener);
    void deleteListener(Listener listener);
    void listen(String command) throws ProjectNotFoundException, TaskNotFoundException, JsonProcessingException, UserNotFoundException;
    void start();
}
