package ru.easton.tm.repository;

import ru.easton.tm.entity.Task;
import ru.easton.tm.exception.TaskNotFoundException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TaskRepository extends AbstractRepository<Task>{

    private static TaskRepository instance = null;

    private TaskRepository(){
    }

    public static TaskRepository getInstance(){
        if (instance == null){
            instance = new TaskRepository();
        }
        return instance;
    }

    @Override
    protected String getName(Task item) {
        return item.getName();
    }

    @Override
    public List<Task> getItems() {
        return super.getItems();
    }

    public Task create(final String name, final Long userId) {
        final var task = new Task(name);
        task.setUserId(userId);
        items.add(task);
        addItemToMap(task);
        return task;
    }

    public Task create(final String name, final String description, final Long userId){
        final var task = new Task(name, description);
        task.setUserId(userId);
        items.add(task);
        addItemToMap(task);
        return task;
    }

    public Task create(final String name, final LocalDateTime endTaskTime, final Long userId){
        final var task = new Task(name, endTaskTime);
        task.setUserId(userId);
        items.add(task);
        addItemToMap(task);
        return task;
    }

    public Task create(final String name, final String description, final LocalDateTime endTaskTime, final Long userId){
        final var task = new Task(name, description, endTaskTime);
        task.setUserId(userId);
        items.add(task);
        addItemToMap(task);
        return task;
    }

    public Task update(final Long id, final String name, final String description){
        final Optional<Task> optionalTask = findById(id);
        if(optionalTask.isEmpty()) return null;
        var task = optionalTask.get();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    public void clear(final Long userId) throws TaskNotFoundException {
        List<Task> userTasks = findByUserId(userId);
        if(userTasks.isEmpty()) throw new TaskNotFoundException("You don't have any project");
        for(final Task task: userTasks) {
            itemsMap.get(task.getName()).remove(task);
            if(itemsMap.get(task.getName()).isEmpty())
                itemsMap.remove(task.getName());
            items.remove(task);
        }
    }

    public List<Task> findByUserId(final Long userId) {
        List<Task> userTasks = new ArrayList<>();
        for(final Task task: items){
            if(task.getUserId() == null) continue;
            if(task.getUserId().equals(userId)) userTasks.add(task);
        }
        return userTasks;
    }

    public Task findByIndex(int index, final Long userId) throws TaskNotFoundException{
        List<Task> userTasks = findByUserId(userId);
        if(index > userTasks.size() - 1) throw new TaskNotFoundException("Entered index more than number of your tasks");
        return userTasks.get(index);
    }

    public Task findByIndex(int index){
        if(index > items.size() - 1) return null;
        return items.get(index);
    }

    public Task findByName(final String name, final List<Task> userTasks){
        for(final Task task: userTasks){
            if(task.getName().equals(name)) return task;
        }
        return null;
    }

    public List<Task> findByName(final String name, final Long userId) throws TaskNotFoundException{
        List<Task> tasksByName = itemsMap.get(name);
        if(tasksByName == null) throw new TaskNotFoundException("Tasks with entered name not found");
        List<Task> result = new ArrayList<>();
        for(final Task task: tasksByName){
            if(task.getUserId()==null) continue;
            if(task.getUserId().equals(userId)) result.add(task);
        }
        if(result.isEmpty()) throw new TaskNotFoundException(String.format("You don't have tasks with name \"%s\"", name));
        return result;
    }

    public Optional<Task> findById(final Long id){
        for(final Task task: items){
            if(task.getId().equals(id)) return Optional.of(task);
        }
        return Optional.empty();
    }

    public List<Task> findAllByProjectId(final Long projectId, final Long userId){
        final List<Task> result = new ArrayList<>();
        for(final Task task: items){
            final var idProject = task.getProjectId();
            final var idUser = task.getUserId();
            if(idProject == null) continue;
            if(idUser == null) continue;
            if(idProject.equals(projectId) && idUser.equals(userId)) result.add(task);
        }
        return result;
    }

    public List<Task> findAllByProjectId(final Long projectId){
        final List<Task> result = new ArrayList<>();
        for(final Task task: items){
            final var idProject = task.getProjectId();
            if(idProject == null) continue;
            if(idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id){
        for(final Task task: items){
            final var idProject = task.getProjectId();
            if(idProject == null) continue;
            if(!idProject.equals(projectId)) continue;
            if(task.getId().equals(id)) return task;
        }
        return null;
    }

    public void removeByName(final String name, final Long userId) throws TaskNotFoundException{
        boolean remove = items.removeIf(x -> x.getName().equals(name) && x.getUserId().equals(userId));
        if(!remove) throw new TaskNotFoundException(String.format("You don't have tasks with name \"%s\"", name));
    }

    public void removeById(final Long id) throws TaskNotFoundException {
        boolean remove = items.removeIf(x -> x.getId().equals(id));
        if(!remove) throw new TaskNotFoundException(String.format("You don't have tasks with id \"%s\"", id));
    }

    public void removeByIndex(final int index, final Long userId) throws TaskNotFoundException {
        final var task = findByIndex(index, userId);
        if(task==null) throw new TaskNotFoundException(String.format("You don't have tasks with index \"%s\"", index));
        items.remove(task);
    }

}
