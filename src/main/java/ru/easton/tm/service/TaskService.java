package ru.easton.tm.service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.easton.tm.entity.Task;
import ru.easton.tm.exception.TaskNotFoundException;
import ru.easton.tm.repository.TaskRepository;

import java.io.*;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TaskService extends AbstractService{

    private final String fileNameJson = "tasks.json";

    private final String fileNameXml = "tasks.xml";

    private static TaskService instance = null;

    private final TaskRepository taskRepository = TaskRepository.getInstance();

    private final UserService userService = UserService.getInstance();

    private final ProjectTaskService projectTaskService = ProjectTaskService.getInstance();

    private TaskService(){
    }

    public static TaskService getInstance(){
        if (instance == null){
            instance = new TaskService();
        }
        return instance;
    }



    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public Task create(final String name, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name, userId);
    }

    public Task create(String name, String description, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description, userId);
    }

    public Task create(String name, LocalDateTime endTaskTime, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.create(name, endTaskTime, userId);
    }

    public Task create(String name, String description, LocalDateTime endTaskTime, final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description, endTaskTime, userId);
    }

    public Task update(final Long id, final String name, final String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.update(id, name, description);
    }

    public void clear(final Long userId) throws TaskNotFoundException {
        taskRepository.clear(userId);
    }

    public Task findByIndex(final int index, final Long userId) throws TaskNotFoundException {
        if(index < 0) throw new TaskNotFoundException("Entered index less than 0");
        return taskRepository.findByIndex(index, userId);
    }

    public Task findByIndex(final int index) throws TaskNotFoundException {
        if(index < 0) throw new TaskNotFoundException("Entered index less than 0");
        return taskRepository.findByIndex(index);
    }

    public void removeByName(final String name, final Long userId) throws TaskNotFoundException {
        if (name == null || name.isEmpty()) throw new TaskNotFoundException("Project's name can't be empty");
        taskRepository.removeByName(name, userId);
    }

    public void removeById(final Long id) throws TaskNotFoundException {
        if (id == null) throw new TaskNotFoundException("Id can't be null");
        taskRepository.removeById(id);
    }

    public void removeByIndex(final int index, final Long userId) throws TaskNotFoundException {
        if(index < 0) throw new TaskNotFoundException("Entered index less than 0");
        taskRepository.removeByIndex(index, userId);
    }

    public List<Task> findByUserId(Long userId) {
        return taskRepository.findByUserId(userId);
    }

    public List<Task> findByName(String name, Long userId) throws TaskNotFoundException {
        if(name == null || name.isEmpty()) throw new TaskNotFoundException("Project's name can't be empty");
        return taskRepository.findByName(name, userId);
    }

    public int createTask(){
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("[CREATE TASK]");
        System.out.println("PLEASE, ENTER TASK NAME: ");
        final var name = scanner.nextLine();
        System.out.println("PLEASE, ENTER TASK DESCRIPTION: ");
        final var description = scanner.nextLine();
        create(name, description, userId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByName() throws TaskNotFoundException {
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("[REMOVE TASK BY NAME]");
        System.out.println("PLEASE, ENTER TASK NAME: ");
        final var name = scanner.nextLine();
        removeByName(name, userId);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskById() throws TaskNotFoundException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("PLEASE, ENTER TASK ID: ");
        Long id = null;
        if(scanner.hasNextLong())
            id = scanner.nextLong();
        removeById(id);
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskByIndex() throws TaskNotFoundException {
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("PLEASE, ENTER TASK INDEX: ");
        final int index = scanner.nextInt() - 1;
        removeByIndex(index, userId);
        System.out.println("[OK]");
        return 0;
    }

    public int updateTaskByIndex() throws TaskNotFoundException {
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER, TASK INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = findByIndex(index, userId);
        if(task == null){
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER PROJECT NAME: ");
        final var name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION: ");
        final var description = scanner.nextLine();
        update(task.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int clearTask() throws TaskNotFoundException {
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("[CLEAR TASK]");
        clear(userId);
        System.out.println("[OK]");
        return 0;
    }

    private void viewTask(final Task task){
        if(task == null) return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    private void viewTasks(final List<Task> tasks){
        for(final Task task: tasks) {
            System.out.println("[VIEW PROJECT]");
            System.out.println("ID: " + task.getId());
            System.out.println("NAME: " + task.getName());
            System.out.println("DESCRIPTION: " + task.getDescription());
            System.out.println("[OK]");
        }
    }

    public int viewTaskByIndex() throws TaskNotFoundException {
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("ENTER, TASK INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = findByIndex(index, userId);
        viewTask(task);
        return 0;
    }

    public int viewTaskByName() throws TaskNotFoundException {
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("PLEASE, ENTER TASK NAME:");
        final var name = scanner.nextLine();
        final List<Task> tasks = findByName(name, userId);
        viewTasks(tasks);
        return 0;
    }

    public int listTask(){
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("[LIST TASK]");
        final List<Task> tasks = findByUserId(userId);
        Collections.sort(tasks, Comparator.comparing(Task::getName));
        int index = 1;
        for(final Task task: tasks){
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public void listTasks(final List<Task> tasks){
        int index = 1;
        for(final Task task: tasks){
            System.out.println(index + ". " + task.getId() + ": " + task.getName());
            index++;
        }
    }

    public int listTasksByProjectId(){
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("[LIST TASKS BY PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT ID: ");
        final var projectId = Long.parseLong(scanner.nextLine());
        final List<Task> tasks = projectTaskService.findAllByProjectId(projectId, userId);
        if(tasks == null || tasks.isEmpty()) {
            System.out.println("[FAIL]");
            return 0;
        }
        listTasks(tasks);
        System.out.println("[OK]");
        return 0;
    }

    public int addTaskToProjectById(){
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("[ADD TASK TO PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID: ");
        final var projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID: ");
        final var taskId = Long.parseLong(scanner.nextLine());
        final Task task = projectTaskService.addTaskToProject(projectId, taskId, userId);
        if(task == null){
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int removeTaskFromProjectById(){
        final var userId = userService.getCurrentUser().getId();
        if(checkAuthentication(userId)) return 0;
        System.out.println("[REMOVE TASK FROM PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID: ");
        final var projectId = Long.parseLong(scanner.nextLine());
        System.out.println("PLEASE, ENTER TASK ID: ");
        final var taskId = Long.parseLong(scanner.nextLine());
        projectTaskService.removeTaskFromProject(projectId, taskId, userId);
        return 0;
    }

    public int saveTasks() throws JsonProcessingException {
        saveTasksAsJson();
        saveTasksAsXml();
        System.out.println("[OK]");
        return 0;
    }

    public void saveTasksAsJson() throws JsonProcessingException {
        var objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(TaskService.getInstance().getTaskRepository().getItems());
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileNameJson))){
            objectOutputStream.writeUTF(json);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveTasksAsXml(){
        var xmlMapper = new XmlMapper();
        try {
            xmlMapper.writerWithDefaultPrettyPrinter().writeValue(new File(fileNameXml), TaskService.getInstance().getTaskRepository().getItems());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int importTasks() {
        String json;
        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileNameJson))) {
            json = objectInputStream.readUTF();
            var objectMapper = new ObjectMapper();
            objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
            List<Task> tasks = Arrays.asList(objectMapper.readValue(json, Task[].class));
            taskRepository.setItems(tasks);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("[OK]");
        return 0;
    }
}
